package eu.number26.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository repository;

    @Override
    public void save(Long id, Transaction transaction) {
        transaction.setId(id);
        repository.save(transaction);
    }

    @Override
    public List<Transaction> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Long> findByType(String type) {
        return repository.findByType(type).stream()
            .map(Transaction::getId)
            .collect(toList());
    }

    @Override
    public Double transitiveSum(Long id) {
        return repository.findByIdOrParentId(id, id).stream()
            .mapToDouble(Transaction::getAmount)
            .sum();
    }
}
