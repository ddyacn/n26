package eu.number26.task;

import java.util.List;

public interface TransactionService {

    void save(Long id, Transaction transaction);

    List<Transaction> findAll();

    List<Long> findByType(String type);

    Double transitiveSum(Long id);
}
