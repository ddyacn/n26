package eu.number26.task;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findByType(String type);

    List<Transaction> findByIdOrParentId(Long id, Long parentId);
}
