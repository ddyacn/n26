package eu.number26.task;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@Entity
public class Transaction {
    @Id
    private Long id;

    protected Long parentId;

    private String type;

    private Double amount;

    public Transaction(Long id, Long parentId, String type, Double amount) {
        this.id = id;
        this.parentId = parentId;
        this.type = type;
        this.amount = amount;
    }
}
