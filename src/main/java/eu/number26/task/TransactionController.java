package eu.number26.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Slf4j
@RestController
@RequestMapping("/transactionservice")
public class TransactionController {

    @Autowired
    private TransactionService service;

    @RequestMapping(method = PUT, value = "/transactions/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void create(@PathVariable Long id, @RequestBody Transaction transaction) {
        service.save(id, transaction);
    }

    @RequestMapping(method = GET, value = "/transactions")
    public List<Transaction> findAll() {
        return service.findAll();
    }

    @RequestMapping(method = GET, value = "/types/{type}")
    public List<Long> findByType(@PathVariable String type) {
        return service.findByType(type);
    }

    @RequestMapping(method = GET, value = "/sum/{id}")
    public Map<String, Double> transitiveTransactionsSum(@PathVariable Long id) {
        Map<String, Double> response = new HashMap<>();
        response.put("sum", service.transitiveSum(id));
        return response;
    }
}
