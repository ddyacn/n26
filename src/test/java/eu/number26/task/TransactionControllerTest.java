package eu.number26.task;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class TransactionControllerTest {

    @Autowired
    TransactionRepository repository;

    @Autowired
    TransactionController controller;

    MockMvc mvc;

    @Before
    public void createMvc() {
        MockitoAnnotations.initMocks(this);
        mvc = standaloneSetup(controller).build();
    }

    @Test
    public void shouldAddNewTransactions() throws Exception {
        String json = "{ \"amount\": 5000, \"type\":\"cars\" }";
        mvc.perform(
            put("/transactionservice/transactions/{id}", 1)
                .contentType(APPLICATION_JSON)
                .content(json.getBytes()))
            .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnTransactionsByType() throws Exception {
        when(repository.findByType("cars"))
            .thenReturn(singletonList(new Transaction(1L, null, "cars", 11.0)));

        when(repository.findByType("shop"))
            .thenReturn(asList(
                new Transaction(2L, null, "shop", 17.0),
                new Transaction(3L, 2L, "shop", 19.0)));

        mvc.perform(get("/transactionservice/types/personal"))
            .andExpect(jsonPath("$.length()", is(0)));

        mvc.perform(get("/transactionservice/types/cars"))
            .andExpect(jsonPath("$.length()", is(1)))
            .andExpect(jsonPath("$.[0]", is(1)));

        mvc.perform(get("/transactionservice/types/shop"))
            .andExpect(jsonPath("$.length()", is(2)))
            .andExpect(jsonPath("$.[0]", is(2)))
            .andExpect(jsonPath("$.[1]", is(3)));
    }

    @Test
    public void shouldReturnTransitiveSum() throws Exception {
        when(repository.findByIdOrParentId(11L, 11L))
            .thenReturn(singletonList(new Transaction(11L, 10L, "shopping", 10000.0)));

        when(repository.findByIdOrParentId(10L, 10L))
            .thenReturn(asList(
                new Transaction(10L, null, "cars", 5000.0),
                new Transaction(11L, 10L, "shopping", 10000.0)));

        mvc.perform(get("/transactionservice/sum/{id}", 10))
            .andExpect(jsonPath("$.sum", is(15000.0)));

        mvc.perform(get("/transactionservice/sum/{id}", 11))
            .andExpect(jsonPath("$.sum", is(10000.0)));
    }

    @Test
    public void shouldReturnTransactions() throws Exception {
        when(repository.findAll())
            .thenReturn(asList(
                new Transaction(1L, null, "cars", 11.0),
                new Transaction(2L, null, "shop", 17.0),
                new Transaction(3L, 2L, "shop", 19.0)));

        mvc.perform(get("/transactionservice/transactions"))
            .andExpect(jsonPath("$.length()", is(3)))
            .andExpect(jsonPath("[0].id", is(1)))
            .andExpect(jsonPath("[0].parentId", nullValue()))
            .andExpect(jsonPath("[0].type", is("cars")))
            .andExpect(jsonPath("[0].amount", is(11.0)))
            .andExpect(jsonPath("[1].id", is(2)))
            .andExpect(jsonPath("[1].parentId", nullValue()))
            .andExpect(jsonPath("[1].type", is("shop")))
            .andExpect(jsonPath("[1].amount", is(17.0)))
            .andExpect(jsonPath("[2].id", is(3)))
            .andExpect(jsonPath("[2].parentId", is(2)))
            .andExpect(jsonPath("[2].type", is("shop")))
            .andExpect(jsonPath("[2].amount", is(19.0)));
    }
}
