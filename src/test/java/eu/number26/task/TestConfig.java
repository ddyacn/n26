package eu.number26.task;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.mock;

@Profile("test")
@Configuration
public class TestConfig {

    @Bean
    TransactionRepository transactionRepository() {
        return mock(TransactionRepository.class);
    }

    @Bean
    TransactionService transactionService() {
        return new TransactionServiceImpl();
    }

    @Bean
    TransactionController transactionController() {
        return new TransactionController();
    }
}
